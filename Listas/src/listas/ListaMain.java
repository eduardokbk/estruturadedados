
package listas;

public class ListaMain {

	private static Lista lista;

	public static void main(String[] args) {
		lista = new Lista();
		int tamanho = 10;

		testeInsere(tamanho);

		testeImprime();

		testeToString();

		testeVazia();

		testeBusca();

		testeComprimento();

		testeUltimo();

		testeIgual();

		testeLibera();
	}

	private static void testeImprime() {
		lista.imprime();
	}

	private static void testeInsere(int tamanho) {
		for (int i = 0; i < tamanho; i++) {
			lista.insere(i);
		}
	}

	private static void testeToString() {
		System.out.println(lista);
	}

	private static void testeVazia() {
		System.out.println(lista.vazia());
		System.out.println(new Lista().vazia());
	}

	private static void testeBusca() {
		System.out.println(lista.busca(5).toString());
		System.out.println(lista.busca(lista.comprimento() + 1));
	}

	private static void testeComprimento() {
		System.out.println("O comprimento é: " + lista.comprimento());
	}

	private static void testeUltimo() {
		System.out.println("O ultimo elemento é: " + lista.ultimo());
	}

	private static void testeIgual() {
		Lista lista2 = new Lista();
		for (int i = 0; i < (lista.comprimento() / 2); i++) {
			lista2.insere(i);
		}

		if (lista.igual(lista2))
			System.out.println(lista + " é igual a: " + lista2);
		else
			System.out.println(lista + " é diferente de: " + lista2);

		if (lista.igual(lista))
			System.out.println(lista + " é igual a: " + lista);
		else
			System.out.println(lista + " é diferente de: " + lista);
	}

	private static void testeLibera() {
		lista.libera();
		System.out.println("O comprimento é: " + lista.comprimento());
	}

}

	

	

	

	

	

 
	

	 

	

	 
	

