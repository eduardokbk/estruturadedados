package listas;

public class Lista {
	public void Lista() {
		prim = null;
	}

	private NoLista prim;

	public void insere(int info) {
		NoLista novo = new NoLista();
		novo.setInfo(info);
		novo.setProx(prim);
		this.prim = novo;
	}

	public void imprime() {
		NoLista p = prim;

		while (p != null) {
			System.out.println(p.getInfo());
			p = p.getProx();
		}
	}

	public boolean vazia() {
		return prim == null;
	}

	public NoLista busca(int v) {
		NoLista p = prim;

		while (p != null) {
			if (p.getInfo() == v)
				return p;
			p = p.getProx();
		}
		return null;
	}

	public int comprimento() {
		int length = 0;

		NoLista p = prim;

		while (p != null) {
			length++;
			p = p.getProx();
		}
		return length;
	}

	public NoLista ultimo() {
		NoLista p = prim;

		if (p == null)
			return null;

		while (p.getProx() != null) {
			p = p.getProx();
		}
		return p;
	}

	public String toString() {
		String value = "";
		NoLista p = prim;
		while (p != null) {
			value += p.toString();
			p = p.getProx();
		}
		return value;
	}

	public void retira(int v) {
		NoLista ant = null;
		NoLista p = prim;

		while (p != null && p.getInfo() != v) {
			ant = p;
			p = p.getProx();
		}

		if (p == null)
			return;

		if (ant == null)
			prim = p.getProx();
		else
			ant.setProx(p.getProx());
	}

	public void libera() {
		while(prim!=null){
			retira(ultimo().getInfo());
		}
	}

	public boolean igual(Lista lista) {
		NoLista p1 = prim;
		NoLista p2 = lista.primeiro();

		while (p1 != null && p2 != null) {
			if (p1.getInfo() != p2.getInfo())
				return false;
			p1 = p1.getProx();
			p2 = p2.getProx();
		}

		if (p1 == p2)
			return true;

		return false;
	}

	public NoLista primeiro() {
		return prim;
	}
}

