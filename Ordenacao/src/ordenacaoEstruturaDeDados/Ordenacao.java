package ordenacaoEstruturaDeDados;

public class Ordenacao {

    public static void Order(TipoOrdenacao tipo, int[] array) {
        if (tipo == TipoOrdenacao.BubbleSort) 
             OrderWithBubbleSort(array);

    }

    private static void OrderWithBubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int secondPosition = 1; secondPosition < array.length; secondPosition++) {
                int secondValue = array[secondPosition];
                int firstPosition = secondPosition - 1;

                int firstValue = array[secondPosition - 1];

                if (firstValue > secondValue) {
                    array[firstPosition] = secondValue;
                    array[secondPosition] = firstValue;
                }
            }
        }
    }
}