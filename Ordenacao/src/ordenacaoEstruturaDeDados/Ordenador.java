package ordenacaoEstruturaDeDados;

public class Ordenador {

    public Ordenador() {
    }

    public static void main(String[] args) {
        int[] array = new int[5];
        array[0] = 4;
        array[1] = 5;
        array[2] = 3;
        array[3] = 1;
        array[4] = 2;

        Ordenacao.Order(TipoOrdenacao.BubbleSort, array);
        System.out.print("\nBubbleSort: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }

        Ordenacao.Order(TipoOrdenacao.MergeSort, array);
        System.out.print("\nMergeSort: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }

        Ordenacao.Order(TipoOrdenacao.QuickSort, array);
        System.out.print("\nQuickSort: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
    }
}