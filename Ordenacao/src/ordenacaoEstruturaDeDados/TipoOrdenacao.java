package ordenacaoEstruturaDeDados;

public enum TipoOrdenacao {
    BubbleSort,
    QuickSort,
    MergeSort;
}
